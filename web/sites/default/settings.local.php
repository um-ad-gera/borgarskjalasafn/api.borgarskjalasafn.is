<?php

$config['system.logging']['error_level'] = 'verbose';

// Set this to TRUE or comment out in production
$config['system.performance']['css']['preprocess'] = FALSE;
$config['system.performance']['js']['preprocess'] = FALSE;

$settings['container_yamls'][] = $app_root . '/sites/development.services.yml';

// Ddisable the render cache and disable dynamic page cache:
$settings['cache']['bins']['render'] = 'cache.backend.null';
$settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null';
$settings['cache']['bins']['page'] = 'cache.backend.null';

// Drupal >= 8.8
$settings['config_sync_directory'] = $app_root . '/../config/sync';

$settings['config_exclude_modules'] = ['devel', 'stage_file_proxy'];

$settings['trusted_host_patterns'] = [
  '^.+\.ddev\.site$'
];

// Environment indicator
// name     local    dev      test     stage    live
// fg_color #ffffff  #ffffff  #4d7750  #642828  #acf9ff
// bg_color #000000  #5a5a5a  #1dea59  #f3f200  #ff0000
$config['environment_indicator.indicator']['name'] = 'local';
$config['environment_indicator.indicator']['fg_color'] = '#ffffff';
$config['environment_indicator.indicator']['bg_color'] = '#000000';

// Change kint max_depth setting.
if (class_exists('Kint')) {
  // Set the max_depth to prevent out-of-memory.
  \Kint::$max_depth = 4;
}
