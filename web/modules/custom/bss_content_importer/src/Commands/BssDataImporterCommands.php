<?php
namespace Drupal\bss_content_importer\Commands;

//use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\Core\Entity\EntityStorageException;
use Drush\Commands\DrushCommands;

/**
 * A Drush command file.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 * - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 * - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */

class BssDataImporterCommands extends DrushCommands {
  /**
   * Does some stuff that needs a Drush command
   *
   * @command bss_content_importer:display_warning
   * @aliases display_warning
   * @usage bss_content_importer:display_warning
   *   Displays a warning about how much intruder Reynir is
   */
  public function displayWarning() {
    $this->logger()->warning("Reynir is an intruder!");
  }

  /**
   * Imports news items
   *
   * @param bool $production
   * @command bss_content_importer:import_frettir
   * @aliases import_frettir
   * @usage bss_content_importer:import_frettir
   * Imports frettir from json files in /migrate_documents/json
   */
  public function importFrettir($production = false) {
    $value = bss_content_importer_import_frettir($production);
    $this->logger()->success(dt($value . " fréttir imported."));
  }

  /**
   * Imports fjolskyldan from json files in /migrate_documents/json
   *
   * @param bool $production
   * @command bss_content_importer:import_fjolskyldan
   * @aliases import_fjolskyldan
   * @usage bss_content_importer:import_fjolskyldan
   */
  public function importFjolskyldan($production = false) {
    $value = bss_content_importer_import_fjolskyldan($production);
    $this->logger()->success(dt($value . " Fjölskyldan imported."));
  }

  /**
   * Imports benediktarskjol from json files in /migrate_documents/json
   *
   * @param bool $production
   * @command bss_content_importer:import_benediktarskjol
   * @aliases import_benediktarskjol
   * @usage bss_content_importer:import_benediktarskjol
   */
  public function importBenediktarskjol($production = false) {
    $value = bss_content_importer_import_benediktarskjol($production);
    $this->logger()->success(dt($value . " Benediktarskjöl imported."));
  }

  /**
   * Imports ljosmyndir from json files in /migrate_documents/json
   *
   * @param bool $production
   * @command bss_content_importer:import_ljosmyndir
   * @aliases import_ljosmyndir
   * @usage bss_content_importer:import_ljosmyndir
   */
  public function importLjosmyndir($production = false) {
    $value = bss_content_importer_import_ljosmyndir($production);
    $this->logger()->success(dt($value . " Ljósmyndir imported."));
  }

  /**
   * Imports munir from json files in /migrate_documents/json
   *
   * @param bool $production
   * @command bss_content_importer:import_munir
   * @aliases import_munir
   * @usage bss_content_importer:import_munir
   */
  public function importMunir($production = false) {
    $value = bss_content_importer_import_munir($production);
    $this->logger()->success(dt($value . " Munir imported."));
  }

  /**
   * Imports prentad_efni from json files in /migrate_documents/json
   *
   * @param bool $production
   * @command bss_content_importer:import_prentad_efni
   * @aliases import_prentad_efni
   * @usage bss_content_importer:import_prentad_efni
   */
  public function importPrentadEfni($production = false) {
    $value = bss_content_importer_import_prentad_efni($production);
    $this->logger()->success(dt($value . " Prentað efni imported."));
  }

  /**
   * Imports raedur from json files in /migrate_documents/json
   *
   * @param bool $production
   * @command bss_content_importer:import_raedur
   * @aliases import_raedur
   * @usage bss_content_importer:import_raedur
   */
  public function importRaedur($production = false) {
    $value = bss_content_importer_import_raedur($production);
    $this->logger()->success(dt($value . " Ræður imported."));
  }

  /**
   * Imports stjornmalamadurinn from json files in /migrate_documents/json
   *
   * @param bool $production
   * @command bss_content_importer:import_stjornmalamadurinn
   * @aliases import_stjornmalamadurinn
   * @usage bss_content_importer:import_stjornmalamadurinn
   */
  public function importStjornmalamadurinn($production = false) {
    $value = bss_content_importer_import_stjornmalamadurinn($production);
    $this->logger()->success(dt($value . " Stjórnmálamaðurinn imported."));
  }

  /**
   * Imports uppvaxtar_og_namsar from json files in /migrate_documents/json
   *
   * @param bool $production
   * @command bss_content_importer:import_namsar
   * @aliases import_namsar
   * @usage bss_content_importer:import_namsar
   */
  public function importNamsar($production = false) {
    $value = bss_content_importer_import_uppvaxtar_og_namsar($production);
    $this->logger()->success(dt($value . " Uppvaxtar- og námsár imported."));
  }

  /**
   * Imports borgin-skjol from json files in /migrate_documents/json
   *
   * @param bool $production
   * @command bss_content_importer:import_borgin
   * @aliases import_borgin
   * @usage bss_content_importer:import_borgin
   */
  public function importBorgin($production = false) {
    $value = bss_content_importer_import_borgin($production);
    $this->logger()->success(dt($value . " Borgin skjöl imported."));
  }

  /**
   * Imports borgin-einstaklingar from json files in /migrate_documents/json
   *
   * @param bool $production
   * @command bss_content_importer:import_einstaklingar
   * @aliases import_einstaklingar
   * @usage bss_content_importer:import_einstaklingar
   */
  public function importEinstaklingar($production = false) {
    $value = bss_content_importer_import_einstaklingar($production);
    $this->logger()->success(dt($value . " Einstaklingar skjöl imported."));
  }

  /**
   * Imports borgin-felog from json files in /migrate_documents/json
   *
   * @param bool $production
   * @command bss_content_importer:import_felog
   * @aliases import_felog
   * @usage bss_content_importer:import_felog
   */
  public function importFelog($production = false) {
    $value = bss_content_importer_import_felog($production);
    $this->logger()->success(dt($value . " Félög skjöl imported."));
  }

  /**
   * Imports borgin-fyrirtaeki from json files in /migrate_documents/json
   *
   * @param bool $production
   * @command bss_content_importer:import_fyrirtaeki
   * @aliases import_fyrirtaeki
   * @usage bss_content_importer:import_fyrirtaeki
   */
  public function importFyrirtaeki($production = false) {
    $value = bss_content_importer_import_fyrirtaeki($production);
    $this->logger()->success(dt($value . " Fyrirtæki skjöl imported."));
  }

  /**
   * Imports elstu skjölin from json files in /migrate_documents/json
   *
   * @param bool $production
   * @command bss_content_importer:import_elstu_skjolin
   * @aliases import_elstu_skjolin
   * @usage bss_content_importer:import_elstu_skjolin
   */
  public function importElstuSkjolin($production = false) {
    $value = bss_content_importer_import_elstu_skjolin($production);
    $this->logger()->success(dt($value . " skjöl imported."));
  }

  /**
   * Imports Kaupmannasamtök Íslands from json files in /migrate_documents/json
   *
   * @param bool $production
   * @command bss_content_importer:import_kaupmannasamtok
   * @aliases import_kaupmannasamtok
   * @usage bss_content_importer:import_kaupmannasamtok
   */
  public function importKaupmannasamtok($production = false) {
    $value = bss_content_importer_import_kaupmannasamtok($production);
    $this->logger()->success(dt($value . " skjöl imported."));
  }

  /**
   * Imports all skjalaskrar from json files in /migrate_documents/json
   *
   * @param bool $production
   * @command bss_content_importer:import_skjalaskrar
   * @aliases import_skjalaskrar
   * @usage bss_content_importer:import_skjalaskrar
   */
  public function importSkjalaskrar($production = false) {
    $this->importBorgin($production);
    $this->importEinstaklingar($production);
    $this->importFelog($production);
    $this->importFyrirtaeki($production);
  }

  /**
   * Imports all Bjarni Ben from json files in /migrate_documents/json
   *
   * @param bool $production
   * @command bss_content_importer:import_bjarni
   * @aliases import_bjarni
   * @usage bss_content_importer:import_bjarni
   */
  public function importBjarni($production = false) {
    $this->importBenediktarskjol($production);
    $this->importFjolskyldan($production);
    $this->importLjosmyndir($production);
    $this->importMunir($production);
    $this->importPrentadEfni($production);
    $this->importRaedur($production);
    $this->importStjornmalamadurinn($production);
    $this->importNamsar($production);
  }

  /**
   * Imports Brunabótavirðingar-books from json file
   *
   * @param string $filename
   * @param bool $production
   * @command bss_content_importer:import_brunabotavirdingar_books
   * @aliases import_brunabotavirdingar_books
   * @usage bss_content_importer:import_brunabotavirdingar_books
   */
  public function importBrunabotavirdingarBooks($filename, $production = false) {
    if(strlen($filename) > 3) {
      $value = bss_content_importer_import_brunabotavirdingar_books($production, $filename);
      $this->logger()->success(dt($value . " skjöl imported."));
    }
    else {
      $this->logger()->warning(dt('Filename missing!'));
    }
  }

  /**
   * Imports Brunabótavirðingar from json file
   *
   * @param bool $production
   * @command bss_content_importer:import_brunabotavirdingar
   * @aliases import_brunabotavirdingar
   * @usage bss_content_importer:import_brunabotavirdingar
   */
  public function importBrunabotavirdingar($production = false) {
    $value = bss_content_importer_import_brunabotavirdingar($production);
    $this->logger()->success(dt($value . " skjöl imported."));
  }
}
