<?php
namespace Drupal\bss_insurance_importer\Commands;

//use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\Core\Entity\EntityStorageException;
use Drush\Commands\DrushCommands;

/**
 * A Drush command file.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 * - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 * - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */

class BssInsuranceImporterCommands extends DrushCommands
{
  /**
   * Does some stuff that needs a Drush command
   *
   * @command bss_insurance_importer:insurance_warning
   * @aliases insurance_warning
   * @usage bss_insurance_importer:insurance_warning
   *   Displays a warning about how much intruder Reynir is
   */
  public function displayWarning()
  {
    $this->logger()->warning("Reynir is still an intruder!");
  }

  /**
   * Imports the registry book
   *
   * @param bool $production
   * @command bss_insurance_importer:import_registry
   * @aliases import_registry
   * @usage bss_insurance_importer:import_registry
   *   Imports all images and pdfs
   */
  public function importRegistry(bool $production = false) {
    $value = bss_insurance_importer_import_registry($production);
  }
}
